package com.mycompany.app;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.common.Levenshtein;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class LevenshteinPOC {

    public static final int HOME_PHONE = 1;
    public static final int ROW_COUNT = 5471;
    public static final int SR_ID = 0;
    public static final String FILE_NAME = "src/main/resources/Spam_Fraud_SRs.xlsx";
    public static final String SHEET_NAME = "5k";

    public static void main(String args[]) throws IOException {

        XSSFSheet sheet = openSheet();

        Map<String, String> allRequests = new HashMap<>(ROW_COUNT);
        List<DataTable> tableList = new ArrayList<>();
        Date startTime = new Date();
        for (Row row : sheet) {
            long reqStartTime = System.nanoTime();
            Cell cell = row.getCell(HOME_PHONE);
            if (cell != null) {
                String phoneNumber = extractPhoneNumber(cell);

                if (!phoneNumber.equals("HOME_PHONE") && !(phoneNumber.isEmpty())) {
                    String srId = String.valueOf(BigDecimal.valueOf(row.getCell(SR_ID).getNumericCellValue()).toBigInteger());
                    tableList.add(getDataTable(allRequests, phoneNumber, srId, reqStartTime));
                    allRequests.put(srId, phoneNumber);
                }
            }
            if (row.getRowNum() > ROW_COUNT) break;
        }

        System.out.println("Total Time Taken = " + (new Date().getTime() - startTime.getTime()) / 1000D + " seconds");
        printStatistics(tableList);

    }

    private static String extractPhoneNumber(Cell cell) {
        return cell.getCellType() == CellType.NUMERIC
            ? String.valueOf(BigDecimal.valueOf(cell.getNumericCellValue()).toBigInteger())
            : cell.getStringCellValue().replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
    }

    private static XSSFSheet openSheet() throws IOException {
        XSSFSheet sheet = null;
        try (OPCPackage opcPackage = OPCPackage.open(new File(FILE_NAME))) {
            sheet = new XSSFWorkbook(opcPackage).getSheet(SHEET_NAME);
        } catch (InvalidFormatException e) {
            System.out.println("Error while open the excel sheet: " + e.getMessage());
        }
        return sheet;
    }

    private static void printStatistics(List<DataTable> tableList) {
        tableList.forEach(System.out::println);
        long size = tableList.size();
        System.out.println("\n\nTotal requests: " + size);
        LevenshteinStatistics lsd = new LevenshteinStatistics();
        for (DataTable dataTable : tableList) {
            switch (dataTable.getLevenshteinDistance()) {
                case 0 -> lsd.setDistance0(lsd.getDistance0() + 1);
                case 1 -> lsd.setDistance1(lsd.getDistance1() + 1);
                case 2 -> lsd.setDistance2(lsd.getDistance2() + 1);
                case 3 -> lsd.setDistance3(lsd.getDistance3() + 1);
                case 4 -> lsd.setDistance4(lsd.getDistance4() + 1);
            }
        }
        lsd.printCount(size);
    }

    private static DataTable getDataTable(Map<String, String> allRequests, String phoneNumber, String srId, double startTime) {
        int lowestDistance = 9;
        String srIdCompared = "NO_SR";
        String phoneNumberCompared = "NO_PHONE";
        for (Map.Entry<String, String> entry : allRequests.entrySet())
            if (entry.getValue().substring(0, 6).equals(phoneNumber.substring(0, 6))) {
                int distance = Levenshtein.distance(entry.getValue().substring(6, 10), phoneNumber.substring(6, 10));
                if (distance < lowestDistance) {
                    lowestDistance = distance;
                    srIdCompared = entry.getKey();
                    phoneNumberCompared = entry.getValue();
                }
            }
        return new DataTable(srId, phoneNumber, phoneNumberCompared, lowestDistance, srIdCompared, (System.nanoTime() - startTime)/1000000D);
    }
}
