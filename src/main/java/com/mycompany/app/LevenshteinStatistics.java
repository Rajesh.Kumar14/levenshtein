package com.mycompany.app;

public class LevenshteinStatistics {

    private long distance0 = 0;
    private long distance1 = 0;
    private long distance2 = 0;
    private long distance3 = 0;
    private long distance4 = 0;

    public long getDistance0() {
        return distance0;
    }

    public void setDistance0(long distance0) {
        this.distance0 = distance0;
    }

    public long getDistance1() {
        return distance1;
    }

    public void setDistance1(long distance1) {
        this.distance1 = distance1;
    }

    public long getDistance2() {
        return distance2;
    }

    public void setDistance2(long distance2) {
        this.distance2 = distance2;
    }

    public long getDistance3() {
        return distance3;
    }

    public void setDistance3(long distance3) {
        this.distance3 = distance3;
    }

    public long getDistance4() {
        return distance4;
    }

    public void setDistance4(long distance4) {
        this.distance4 = distance4;
    }

    public void printCount(long total) {
        System.out.println("LD Distance 0 : " + distance0 + " " + (double) distance0 * 100 / total + "%");
        System.out.println("LD Distance 1 : " + distance1 + " " + (double) distance1 * 100 / total + "%");
        System.out.println("LD Distance 2 : " + distance2 + " " + (double) distance2 * 100 / total + "%");
        System.out.println("LD Distance 3 : " + distance3 + " " + (double) distance3 * 100 / total + "%");
        System.out.println("LD Distance 4 : " + distance4 + " " + (double) distance4 * 100 / total + "%");
    }
}
